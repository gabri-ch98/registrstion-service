package main

import (
	"MicroServicies/DB"
	_ "MicroServicies/DB"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"math/rand"
	"net/http"
	_ "text/scanner"
)

type Car struct {
	Ident int `json:"id,omitempty"`
	Brand string `json:"brand,omitempty"`
	Model string `json:"model,omitempty"`
	Horses string `json:"horse_power,omitempty"`
}

var id int
var err error

func main() {
	var con DB.Connection
	con.Conn = DB.Connect()
	defer con.Conn.Close()

	http.HandleFunc("/service/v1/cars", func(w http.ResponseWriter, r *http.Request) {

		if r.Method == "GET"{
			fmt.Fprint(w,"This service only accepts POST method")
		}else {
			r.Body = http.MaxBytesReader(w, r.Body, 1048576)
			dec := json.NewDecoder(r.Body)
			var carless Car
			_ = dec.Decode(&carless)

			id,err = DB.LastID(con)
			if err != nil{
				id = 1
			}else {
				id = id+rand.Intn(9)+1
			}
			carless.Ident = id

			err = DB.RegCar(con,DB.Car(carless))
			if err != nil {
				fmt.Fprint(w, "An error related to the DB has occurred, please try to register your car again.")
			} else {
				json.NewEncoder(w).Encode(carless)
			}
		}
	})
	http.ListenAndServe(":8081", nil)
}
